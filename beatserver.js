var DDPServer = require('ddp-server-reactive');
var exec = require('child_process').exec;
var cpu = require('cpu-load');

// Create a server listening on the default port 3000
var server = new DDPServer();

// Create a reactive collection
// All the changes below will automatically be sent to subscribers
var stepvalues = server.publish('stepvalues');

// Add items
var i = 0,
    j = 0,
    sounds = ['kick','snare','rim','hihat','hihat2','crash','ride','clap'],
    steps = 32;//[false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

for (i; i < sounds.length; i += 1) {
    j = 0;
    for (j; j < steps; j += 1) {
        stepvalues[sounds[i] + '-' + j] = {value: false};
    }
}

// Add methods
server.methods({
    click: function(id, value) {
        stepvalues[id].value = value;
        return true;
    }
});
